import asyncio
import websockets

def main():
    # Create handler for each connection
    async def handler(websocket, path): 
        async for message in websocket:        
            reply = f"Data recieved as:  {message}!" 
            print(reply)
            await websocket.send(reply)

    print("Main function")    
    start_server = websockets.serve(handler, "127.0.0.1", 8765)
    asyncio.get_event_loop().run_until_complete(start_server) 
    asyncio.get_event_loop().run_forever()

if __name__ == "__main__":
    main()
