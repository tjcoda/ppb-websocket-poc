import ppb
from ppb import keycodes
from ppb.events import KeyPressed, KeyReleased

from systems import events

class Player(ppb.Sprite):
    print("Player initialized")
    left = keycodes.Left
    right = keycodes.Right
    position = ppb.Vector(0, -3)
    direction = ppb.Vector(0, 0)
    speed = 4

    def on_update(self, update_event, signal):
        self.position += self.direction * self.speed * update_event.time_delta

    def on_key_pressed(self, key_event: KeyPressed, signal):
        if key_event.key == self.left:
            print("Left key pressed")
            self.direction += ppb.Vector(-1, 0)
            signal(events.ClientToServerNetworkEvent(data=["left"]))
        elif key_event.key == self.right:
            print("Right key pressed")
            self.direction += ppb.Vector(1, 0)
            signal(events.ClientToServerNetworkEvent(data=["right"]))

    def on_key_released(self, key_event: KeyReleased, signal):
        if key_event.key == self.left:
            self.direction += ppb.Vector(1, 0)
        elif key_event.key == self.right:
            self.direction += ppb.Vector(-1, 0)   
