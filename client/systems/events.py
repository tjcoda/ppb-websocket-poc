from dataclasses import dataclass
from typing import List

@dataclass
class ClientToServerNetworkEvent:
    """
    A game logic event that is sent from the client to the server.
    """
    data: List[str]