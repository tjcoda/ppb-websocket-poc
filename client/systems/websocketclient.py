from ppb import GameEngine
from ppb import events
from ppb.systemslib import System
from ppb.events import Update
import rel
import _thread
import json

from websockets import connect
import asyncio
import websockets
import threading

URL = 'ws://127.0.0.1:8765'

class WebsocketClientSystem(System):
    

    def set_interval(self, func, sec):
        def func_wrapper():
            self.set_interval(func, sec)
            func()
        t = threading.Timer(sec, func_wrapper)
        t.start()
        return t
    
    def keep_alive(self):
        self.command(["keep_alive"])


    def __init__(
            self,
            *,
            engine: GameEngine,
            **kwargs,
        ):
        super().__init__(engine=engine, **kwargs)
        print("WebsocketClientSystem initializing...")
        self.ws = None
        self.loop = asyncio.get_event_loop()
        # perform a synchronous connect
        self.loop.run_until_complete(self.__async__connect())
        print("WebsocketClientSystem initialized")
        
    async def __async__connect(self):
        print("attempting connection to {}".format(URL))
        # perform async connect, and store the connected WebSocketClientProtocol
        # object, for later reuse for send & recv
        self.ws = await connect(URL)

        self.set_interval(self.keep_alive, 3)
        
        print("connected")

    def command(self, cmd):
        return self.loop.run_until_complete(self.__async__command(cmd))

    async def __async__command(self, cmd):
        await self.ws.send(json.dumps(cmd))
        return await self.ws.recv()

    def on_client_to_server_network_event(self, event, signal):
        print(f"WebsocketClientSystem received {event}")
        foo = self.command(event.data)
        print('server says: ', foo)  
    
