import ppb
from ppb import keycodes
from ppb.events import KeyPressed, KeyReleased
from ppb.features.default_sprites import TargetSprite

from player import Player

from systems.websocketclient import WebsocketClientSystem

def setup(scene):
    scene.add(Player(position=(0, -7)))

ppb.run(
    title="Websocket PoC",
    setup=setup,
    systems=[WebsocketClientSystem]
)